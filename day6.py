from re import findall

instructions = open('light_instructions.txt', 'r').read()
some_list = [[0 for i in range(1000)] for j in range(2000)]

inst = findall(r"(toggle|turn on|turn off)\s(\d*),(\d*)\sthrough\s(\d*),(\d*)", instructions)
for action, x1, y1, x2, y2 in inst:
    coords = [(x, y) for x in range(int(x1), int(x2) + 1) for y in range(int(y1), int(y2) + 1)]
    for x, y in coords:
        if action == 'turn on':
            some_list[x][y] = 1
            some_list[x + 1000][y] += 1
        elif action == 'turn off':
            some_list[x][y] = 0
            if some_list[x + 1000][y]:
                some_list[x + 1000][y] -= 1
        else:
            some_list[x][y] = not some_list[x][y]
            some_list[x + 1000][y] += 2

print(sum([sum(some_list[i]) for i in range(1000)]))
print(sum([sum(some_list[i]) for i in range(1000, 2000)]))
