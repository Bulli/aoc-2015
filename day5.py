strings = open('aoc_strings.txt', 'r').readlines()
nice_strings = 0
nicer_strings = 0

for line in strings:
    counter = 0
    vocal_count = 0
    double_letter_row = 0
    double_letter = 0
    for i in range(0, len(line)):
        for j in range(0, len(line) - 1):
            if line[i:i + 2] == line[j:j + 2] and j > i + 1:
                counter += 1
        if line[i] in 'aeiou':
            vocal_count += 1
        if i < len(line) - 1:
            if line[i] == line[i + 1]:
                double_letter_row += 1
        if i < len(line) - 2:
            if line[i] == line[i + 2]:
                double_letter += 1
    if vocal_count >= 3 and double_letter_row >= 1 and (
    not ('ab' in line or 'cd' in line or 'pq' in line or 'xy' in line)):
        nice_strings += 1
    if counter >= 1 and double_letter >= 1:
        nicer_strings += 1

print(nice_strings)
print(nicer_strings)
